# Zarzwick Unrelated LV2 Plugin(s)

## Pop suppressor

This plugin is an *ad-hoc* plugin to remove pops in a mono signal.
Specifically it tries to find regions where the signal is unaltered except for
an offset in amplitude. This is done by taking a smoothed derivative of the
signal, where only the highest values are kept ; the "pop" regions as well as
their amplitude offset are inferred from the remaining spikes.

![Picture of a pop removed](pop-suppressor-0.webp "Effect of pop suppression.")

This model is really simplist. It suited my needs well ! Well, it suited my
needs decently ; it is not a magical solution. The few parameters are :

- **Pop expected size**: provided as a couple (minimum, maximum). Consider only
    regions whose sample count lies in this range.
- **Gradient threshold**: a *relative* value under which derivatives are
    considered 0. Increasing this value will detect less regions, and
    decreasing it will detect more regions (with much more false positibes).

Additionally the plugin can output the **residual**, for one to check that
no important sounds are lost.

This is **not** considered a realtime plugin. The processing is quite long
(as in not optimized and involving sorting) and also incurs a **delay** which
corresponds to the number of samples per `run()` call.
It is intended to use with **audacity**. In `Manage > Options` you can define
the number of samples yourself, and the software is made for offline
processing, not for realtime effects.

### Building

You'll need the `lv2` library and headers, and the `sndfile` library for
tests. Then :

    zig build -Drelease-safe=true

### Using

At some point I'll try to provide an archive directly. For now you need to
compile it and put the files in some LV2-known place, such as

    cp zig-out/manifest.ttl ~/.lv2/pop-suppressor/manifest.ttl
    cp zig-out/pop-suppressor.ttl ~/.lv2/pop-suppressor/pop-suppressor.ttl
    cp zig-out/lib/libpop-suppressor.so ~/.lv2/pop-suppressor/libpop-suppressor.so

### Outside of linux

It should be easy to port, but currently I've been lazy, and a `.so` is
expected in the manifest. This should be the only thing to modify.

