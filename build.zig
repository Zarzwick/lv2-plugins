const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions() ;
    const kind = std.build.LibExeObjStep.SharedLibKind.unversioned ;

    // Plugins turtles.
    const pop_suppressor_ttl = b.addInstallFile(.{.path = "pop-suppressor/plugin.ttl"}, "pop-suppressor.ttl") ;

    // One turtle to rule them all.
    const bundle_ttl = b.addInstallFile(.{.path = "manifest.ttl.in"}, "manifest.ttl") ;
    pop_suppressor_ttl.step.dependOn(&bundle_ttl.step) ;

    // Pop suppressor.
    const lib = b.addSharedLibrary("pop-suppressor", "pop-suppressor/main.zig", kind) ;
    lib.step.dependOn(&pop_suppressor_ttl.step) ;
    lib.setBuildMode(mode) ;
    lib.linkSystemLibrary("c") ;
    lib.linkSystemLibrary("sndfile") ;
    lib.install() ;

    // Pop suppr. tests.
    const main_tests = b.addTest("pop-suppressor/main.zig") ;
    main_tests.setBuildMode(mode) ;
    main_tests.linkSystemLibrary("c") ;
    main_tests.linkSystemLibrary("m") ;
    main_tests.linkSystemLibrary("ogg") ;
    main_tests.linkSystemLibrary("FLAC") ;
    main_tests.linkSystemLibrary("vorbis") ;
    main_tests.linkSystemLibrary("vorbisenc") ;
    main_tests.linkSystemLibrary("sndfile") ;

    const test_step = b.step("test", "Run library tests") ;
    test_step.dependOn(&main_tests.step) ;
}
