# Just a bunch of functions I try in the REPL.

def mirror(a, b, i):
    if i < a:
        return a + (a - i)
    elif i >= b:
        return b - 1 - (i - b)
    else:
        return i

def forward_derivative(s):
    d = np.zeros_like(s)
    for i in range(len(s) - 1):
        d[i] = s[i+1] - s[i]
    return d
 
# This smoothes the signal with only the k nearest neighbours, for each sample.
def knn_smooth(s, w, k):
    slen = len(s)
    wlen = len(w)
    offs = wlen // 2
    wrap = lambda i : mirror(0, slen, i)
    smooth = np.zeros_like(s)
    for i in range(slen):
        # Compute the nearest neighbours.
        sim = np.zeros_like(w)
        for j in range(wlen):
            sim[j] = 1.0 - np.abs(s[i] - s[wrap(i - offs + j)])
        indices = np.flip(np.argsort(sim))[0:k]
        # Combine their information.
        for j in range(k):
            smooth[i] += s[wrap(i - offs + indices[j])]
        smooth[i] /= k
    return smooth

# This doesn't work, probably because the similarity function is
# not calibrated properly.    
#def threshold_smooth(s, w):
#    slen = len(s)
#    wlen = len(w)
#    offs = wlen // 2
#    wrap = lambda i : mirror(0, slen, i)
#    smooth = np.zeros_like(s)
#    for i in range(slen):
#        # Compute the nearest neighbours.
#        sim = np.zeros_like(w)
#        for j in range(wlen):
#            sim[j] = 1.0 - np.abs(s[i] - s[wrap(i - offs + j)])
#        indices = np.argwhere(np.greater_equal(sim, 0.90)).flatten()
#        k = len(indices)
#        # Combine their information.
#        for j in range(k):
#            smooth[i] += s[wrap(i - offs + indices[j])]
#        smooth[i] /= k
#    return smooth

def isolate_peaks(ds, th):
    low = np.min(ds)
    sup = np.max(ds)
    a = low * th
    b = sup * th
    copy = np.zeros_like(ds)
    for i in range(len(ds)):
        val = ds[i]
        if (a < val) and (val < b):
            copy[i] = 0.0
        else:
            copy[i] = val
    return copy

# A prototype function, not good for production.
#def get_regions_dumb(s, ds):
#    regions = []
#    start = -1
#    amp = 0.0
#    amp_tol = 0.0
#    for i in range(len(s)):
#        if (ds[i] > 0.0) and (start < 0):
#            print("condition 1")
#            start = i + 1
#            amp = s[i]
#            amp_tol = 0.2 * amp
#        if (ds[i] < 0.0) and (start >= 0): # and (np.abs(s[i] - amp) < amp_tol):
#            print("condition 2")
#            regions.append((start, i))
#            start = -1
#    return regions

def get_regions(s, ds):
    regions = []
    inside = False
    start = 0
    eps = 1e-4
    amp = 0.0
    amp_tol = 0.0
    for i in range(len(s)):
        # Detect the start of a region.
        if (not inside) and (np.abs(ds[i]) > eps):
            print("condition 1")
            start = i + 1
            amp = ds[i]
            amp_tol = 0.3 * amp
            inside = True
        # Or operate in the region.
        if inside:
            # If it appears that the derivative is opposed to the peak that
            # triggered the region, it is the end, and we can deduce the
            # offset from the pop.
            if (np.abs(ds[i]) > eps) and (np.abs(ds[i]+amp) < amp_tol):
                print("condition 2")
                regions.append((start, i, amp))
                start = 0
                inside = False
            # Otherwise, check the properties desired inside a region.
            # TODO
    return regions

