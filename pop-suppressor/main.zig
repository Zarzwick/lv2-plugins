const std = @import("std") ;

// This is only for tests.
const c   = @cImport({ @cInclude("lv2/core/lv2.h") ; @cInclude("sndfile.h") ; }) ;

const Handle = c.LV2_Handle ;
const Feature = c.LV2_Feature ;
const Descriptor = c.LV2_Descriptor ;

const minimum_samples_per_window = 256 ;
const neighbourhood_radius = 15 ;
const maximum_k = 16 ;

///
/// Properties of pops to be detected. This plugin is very simplist : it tries
/// to detect regions of a given approximate size (in samples) that never
/// cross zero, are outliers w.r.t the neighbour samples, start and stop with
/// brutal derivatives, and exhibit a low variance internally (hopefully
/// indicating a brief and constant wave).
///
/// Constants are fixed for the specific problem I was trying to solve, and
/// are more or less legitimate at 44.1KHz.
///
pub const PopProperties = struct {
    minimum_samples_count : u32 = 80,
    maximum_samples_count : u32 = 120,
} ;

///
/// Stores simple stats, only for reporting, not for analysis.
///
pub const Stats = struct {
    detected_pops : u32 = 0,
} ;

///
/// A region that exhibits a "sudden voltage change". The change is
/// expected to be constant (in my incredilby simplistic model) and is
/// represented by `amp`.
///
pub const Region = struct {
    start: u32,
    end: u32,
    amp: f32,
} ;

///
/// Stores the state of the analyser. Notably, this ensures that pops occuring
/// on the edges of windows could be detected. This is of course of limited use
/// because the part of the pop on the previous window will remain unchanged,
/// but we still try to suppress a part of those.
///
pub const State = struct {
    regions_count : u32,
    regions_in_window : [8]Region,
    maybe_partial_region_in_last_window : ?Region,
} ;

///
/// This is a single-port plugin, analysing windows one at a time and trying to
/// detect pops.
///
pub const Context = struct {
    enabled  : *const f32,
    residual : *const f32,
    gradient_threshold  : *const f32,
    minimum_window_size : *const f32,
    maximum_window_size : *const f32,
    input  : [*]const f32,
    output : [*]f32,
    props : PopProperties,
    stats : Stats,
    state : State,
    ds : [8192]f32, // This is fixed for audacity default.
} ;

///
/// Ports for the plugin.
///
pub const Ports = enum(u32) {
    enable = 0,
    residual = 1,
    gradient_threshold = 2,
    minimum_window_size = 3,
    maximum_window_size = 4,
    input = 5,
    output = 6,
} ;

///
/// ...
///
fn bool_from_control(control: *const f32) bool { return control.* > 0.0 ; }

///
/// Create a new instance of the plugin using the C allocator.
///
pub export fn instantiate(
    desc: ?*const Descriptor,
    rate: f64,
    bundle_path: ?*const u8,
    features: ?*const ?*const Feature,
) Handle {
    _ = rate ;
    _ = desc ;
    _ = features ;
    _ = bundle_path ;
    const context = std.heap.c_allocator.create(Context) catch unreachable ;
    context.*.props = PopProperties {} ;
    context.*.stats = Stats {} ;
    context.*.state = State
        { .regions_count = 0
        , .regions_in_window = undefined
        , .maybe_partial_region_in_last_window = null
        } ;
    return @ptrCast(Handle, @alignCast(1, context)) ;
}

///
/// Destroy the instance.
///
pub export fn cleanup(instance: Handle) void {
    var context = @ptrCast(*Context, @alignCast(@alignOf(*Context), instance)) ;
    std.heap.c_allocator.destroy(context) ;
}

///
/// Reset eventual internal state, except for ports locations.
///
pub export fn activate(instance: Handle) void {
    _ = instance ;
}

///
/// Signal to the plugin that no more run() will be called before activate().
///
pub export fn deactivate(instance: Handle) void {
    _ = instance ;
}

///
/// Connect a specific port.
///
pub export fn connect_port(
    instance: Handle,
    port: u32,
    maybe_data: ?*anyopaque
) void {
    var context = @ptrCast(*Context, @alignCast(@alignOf(*Context), instance)) ;
    // Some weird alignment considerations make me `ptr -> int -> ptr`.
    if (maybe_data) |data| {
        const temp_int = @ptrToInt(data) ;
        switch (@intToEnum(Ports, port)) {
            Ports.enable              => context.enabled             = @intToPtr(*const f32,   temp_int),
            Ports.residual            => context.residual            = @intToPtr(*const f32,   temp_int),
            Ports.gradient_threshold  => context.gradient_threshold  = @intToPtr(*const f32,   temp_int),
            Ports.minimum_window_size => context.minimum_window_size = @intToPtr(*const f32,   temp_int),
            Ports.maximum_window_size => context.maximum_window_size = @intToPtr(*const f32,   temp_int),
            Ports.input               => context.input               = @intToPtr([*]const f32, temp_int),
            Ports.output              => context.output              = @intToPtr([*]f32,       temp_int),
        }
    }
    // Don't consider a NULL pointer as unreachable code. For instance,
    // lv2apply calls this function a first time with null pointers for
    // each port. Don't ask me why.
}

///
/// A helper function to handle signal boundaries. Casts are guaranteed to
/// succeed because of the preconditions (0 <= a < b) and (i < 2 * b).
///
fn mirror(a: u32, b: u32, i: i64) u32 {
    if (i < a) {
        return @intCast(u32, a + (a - i)) ;
    } else if (i >= b) {
        return @intCast(u32, b - 1 - (i - b)) ;
    } else {
        return @intCast(u32, i) ;
    }
}

fn compare_sims(similarities: [*]const f32, lhs: u32, rhs: u32) bool {
    return similarities[lhs] < similarities[rhs] ;
}

///
/// A a precondition, k < (2 * neighbourhood_radius + 1).
///
fn smooth_with_knn(in: [*]const f32, out: [*]f32, nb_samples: u32, k: u32) void {
    const offset = neighbourhood_radius ;
    const window = offset * 2 + 1 ;
    //var w :       [window]f32 = undefined ;
    var sim :     [window]f32 = undefined ;
    var indices : [window]u32 = undefined ;
    var i : u32 = 0 ;
    while (i < nb_samples) : (i += 1) {
        // Reset the similarities to zero and recompute.
        for (sim) |*value| {
            value.* = 0.0 ;
        }
        var j : u32 = 0 ;
        while (j < window) : (j += 1) {
            sim[j] = 1.0 - @fabs(in[i] - in[mirror(0, nb_samples, @intCast(i64, i) - offset + j)]) ;
        }
        // Sort the indices according to the similarities (argsort).
        for (indices) |*index, l| {
            index.* = @intCast(u32, l) ;
        }
        std.sort.sort(u32, indices[0..], @ptrCast([*]const f32, &sim), compare_sims) ;
        // Compute the mean of all these neighbours.
        j = 0 ;
        out[i] = 0.0 ;
        while (j < k) : (j += 1) {
            out[i] += in[mirror(0, nb_samples, @intCast(i64, i) - offset + indices[window-1-j])] ;
        }
        out[i] /= @intToFloat(f32, k) ;
    }
}

///
/// Compute derivatives.
///
fn forward_derivative(s: [*]const f32, ds: [*]f32, nb_samples: u32) void {
    var i : u32 = 0 ;
    while (i < nb_samples - 1) : (i += 1) {
        ds[i] = s[i + 1] - s[i] ;
    }
}

///
/// This sets everything to zero, except for "peaks".
///
fn isolate_peaks(ds: []f32, threshold: f32) void {
    var min : f32 =  1000.0 ;
    var max : f32 = -1000.0 ;
    for (ds) |delta| {
        min = @minimum(delta, min) ;
        max = @maximum(delta, max) ;
    }
    //min = @minimum(min, -1e-2) ; // ...
    //max = @maximum(max,  1e-2) ; // ...
    min *= threshold ;
    max *= threshold ;
    // Apply the reverse thresholding.
    for (ds) |*delta| {
        if ((min < delta.*) and (delta.* < max)) {
            delta.* = 0.0 ;
        }
    }
    // // Second windowed version (not working as expected).
    // const window = 4 ;
    // if (window > ds.len) {
    //     return ;
    // }
    // var d = @ptrCast([*]f32, &ds[0]) ; // Better ?
    // var i : u32 = 0 ;
    // var acc : f32 = 0.0 ;
    // // Fill the accumulator for the first window.
    // while (i < window) : (i += 1) {
    //     acc += d[i] ;
    // }
    // // Shift the window, modifying the accumulator.
    // while (i < ds.len - window) : (i += 1) {
    //     const current = acc ;
    //     acc += d[i] - d[i - window] ;
    //     if ((min < current) and (current < max)) {
    //         d[i - window] = 0.0 ;
    //     }
    // }
}

///
/// Identifies regions from a preprocessed gradient.
///
fn get_regions(context: *Context) void {
    const epsilon = 1e-4 ;
    var inside = false ;
    var start : u32 = undefined ;
    var amp : f32 = undefined ;
    var tol : f32 = undefined ;
    var len : u32 = undefined ;
    // // Handle the case where a previous frame was in.
    //if (context.state.maybe_partial_region_in_last_window) |region| {
    //    std.debug.print("Partial region handled...\n", .{}) ;
    //    inside = true ;
    //    amp = region.amp ;
    //    tol = 0.3 * amp ;
    //}
    // Traverse the derivatives and check.
    for (context.ds) |delta, i| {
        if ((! inside) and (@fabs(delta) > epsilon)) {
            start = @intCast(u32, i) + 1 ;
            amp = delta ;
            tol = 0.3 * amp ;
            len = 0 ;
            inside = true ;
        }
        if (inside) {
            len += 1 ;
            if ((@fabs(delta) > epsilon) and (@fabs(delta + amp) < tol)) {
                // Check window size.
                if (len < @floatToInt(u32, context.minimum_window_size.*)) {
                    inside = false ;
                    continue ;
                }
                // Append region.
                //std.debug.print("Found region ([{}-{}[, +{}).\n", .{start, i+1, amp}) ;
                context.state.regions_in_window[context.state.regions_count] = Region
                    { .start = start
                    , .end = @intCast(u32, i + 1)
                    , .amp = amp
                    } ;
                inside = false ;
                context.state.regions_count += 1 ;
            }
            if (len > @floatToInt(u32, context.maximum_window_size.*)) {
                inside = false ;
            }
        }
    }
    // // Handle the case of an unfinished region.
    //if (inside) {
    //    std.debug.print("Partial region detected...\n", .{}) ;
    //    context.state.maybe_partial_region_in_last_window = Region
    //        { .start = start
    //        , .end = undefined
    //        , .amp = amp
    //        } ;
    //}
    context.stats.detected_pops += context.state.regions_count ;
}

///
/// Compensate the amplitude sudden offsets, or isolate the residual.
///
fn process_regions(context: *Context, nb_samples: u32) void {
    const count = context.state.regions_count ;
    const residual = bool_from_control(context.residual) ;
    if (residual) {
        for (context.output[0..nb_samples]) |*val| {
            val.* = 0.0 ;
        }
        for (context.state.regions_in_window[0..count]) |region| {
            for (context.output[region.start..region.end]) |*val| {
                val.* += region.amp ;
            }
        }
    } else {
        for (context.state.regions_in_window[0..count]) |region| {
            std.debug.print("Removing +{} from region [{}, {}[...\n", .{region.amp, region.start, region.end}) ;
            for (context.output[region.start..region.end]) |*val| {
                val.* -= region.amp ;
            }
        }
    }
}

///
/// Proceed by computing a smoothed version of the signal, computing the
/// derivative of this approximation, thresholding the derivative, and
/// getting the regions from these.
///
fn detect_with_derivative_threshold(context: *Context, nb_samples: u32) void {
    smooth_with_knn(context.input, context.output, nb_samples, 12) ;
    forward_derivative(context.output, @ptrCast([*]f32, &context.ds), nb_samples) ;
    std.mem.copy(f32, context.output[0..nb_samples], context.input[0..nb_samples]) ;
    isolate_peaks(context.ds[0..], context.gradient_threshold.*) ;
    get_regions(context) ;
    process_regions(context, nb_samples) ;
    context.state.regions_count = 0 ;
}

///
/// Run on a window of samples. This assumes the ports are connected, which I
/// think is guaranteed by the host :>. And also assumes that the handle is
/// non-null.
///
pub export fn run(instance: Handle, nb_samples: u32) void {
    // Copy input to output.
    var context = @ptrCast(*Context, @alignCast(@alignOf(*Context), instance)) ;
    std.mem.copy(f32, context.output[0..nb_samples], context.input[0..nb_samples]) ;
    // If enough samples, proceed to detection and correction.
    const enabled = true ; // bool_from_control(context.enabled) ; // FIXME NULL PORT
    if (enabled and (nb_samples >= minimum_samples_per_window)) {
        detect_with_derivative_threshold(context, nb_samples) ;
    }
}

///
/// Is this even useful ? FIXME.
///
pub export fn extension_data(uri: ?*const u8) ?*anyopaque {
    _ = uri ;
    return @intToPtr(?*anyopaque, 0) ;
}

// Static information for the plugin.
const plugin_uri = "http://zarzwickmangrove.fr/lv2/pop-suppressor" ;
var descriptor = c.LV2_Descriptor
    { .URI = plugin_uri
    , .instantiate = instantiate
    , .connect_port = connect_port
    , .activate = activate
    , .run = run
    , .deactivate = deactivate
    , .cleanup = cleanup
    , .extension_data = extension_data
    } ;

pub export fn lv2_descriptor(index: u32) ?*c.LV2_Descriptor {
    return if (index == 0) &descriptor else null ;
}

// zig test main.zig -I/usr/include -lsndfile -lvorbisenc -lm -lFLAC -lopus -lvorbis -logg -lc
test "detection" {
    std.debug.print("\n", .{}) ;
    // Buffers for the windows.
    const window_size = 400 ; // minimum_samples_per_window ;
    var window_in :  [window_size]f32 = undefined ;
    var window_out : [window_size]f32 = undefined ;
    var control_enabled = [2]f32 { 1.0, undefined } ; // TODO Understand the alignment thing...
    var control_residual = [2]f32 { -1.0, undefined } ;
    var control_gradient_threshold = [2]f32 { 0.5, undefined } ;
    var control_minimum_window_size = [2]f32 { 10.0, undefined } ;
    var control_maximum_window_size = [2]f32 { 200.0, undefined } ;
    // Loop over different test "scenes".
    var filename_buffer : [128]u8 = undefined ;
    const filenames = [_][]const u8
        { "samples/pop0,len553.wav"
        , "samples/pop1,len427.wav"
        , "samples/pop2,len548,double.wav"
        } ;
    const expected = [_]u32 { 1, 1, 2 } ;
    for (filenames) |filename, j| {
        std.debug.print("Testing against file {s}...\n", .{filename}) ;
        // Open the WAV file. It is expected to be in float.
        var info: c.SF_INFO = undefined ; info.format = 0 ;
        var file = c.sf_open(&filename[0], c.SFM_READ, &info) ;
        defer _ = c.sf_close(file) ;
        // Ensure the file is mono-channel.
        if (info.channels > 1) {
            return error.BadFileType ;
        }
        // Open a destination file to plot output values.
        std.mem.copy(u8, filename_buffer[0..filename.len], filename[0..]) ;
        filename_buffer[filename.len + 0] = '.' ;
        filename_buffer[filename.len + 1] = 'b' ;
        filename_buffer[filename.len + 2] = 'i' ;
        filename_buffer[filename.len + 3] = 'n' ; // Ouch.
        //PLOTvar plot = try std.fs.cwd().createFile(filename_buffer[0..filename.len + 4], .{}) ;
        //PLOTdefer plot.close() ;
        //PLOTvar stream = std.io.StreamSource { .file = plot } ;
        //PLOTvar writer = stream.writer() ;
        // Create a plugin instance, and connect port.
        var instance = instantiate(null, 1.0, null, null) ;
        defer cleanup(instance) ;
        connect_port(instance, @enumToInt(Ports.enable), &control_enabled) ;
        connect_port(instance, @enumToInt(Ports.residual), &control_residual) ;
        connect_port(instance, @enumToInt(Ports.gradient_threshold), &control_gradient_threshold) ;
        connect_port(instance, @enumToInt(Ports.minimum_window_size), &control_minimum_window_size) ;
        connect_port(instance, @enumToInt(Ports.maximum_window_size), &control_maximum_window_size) ;
        connect_port(instance, @enumToInt(Ports.input), &window_in) ;
        connect_port(instance, @enumToInt(Ports.output), &window_out) ;
        // Compute the number of samples (or frames, this is mono).
        const track_len = @intCast(u32, c.sf_seek(file, 0, c.SEEK_END)) ;
        var remaining = track_len ;
        _ = c.sf_seek(file, 0, c.SEEK_SET) ;
        // Call the run function as many times as required.
        var i : u32 = 0 ;
        while (i < track_len) {
            // Copy signal to the window buffer.
            const len = @minimum(window_size, remaining) ;
            _ = c.sf_read_float(file, &window_in, len) ;
            // Run the plugin.
            run(instance, len) ;
            // Output values in the plotting file.
            //PLOTfor (window_out[0..len]) |value| {
            //PLOT    try std.fmt.format(writer, "{}\n", .{value}) ;
            //PLOT}
            remaining -= len ;
            i += len ;
        }
        // Check the number of detected pops.
        const context = @ptrCast(*Context, @alignCast(@alignOf(*Context), instance)) ;
        try std.testing.expect(expected[j] == context.stats.detected_pops) ;
    }
}

